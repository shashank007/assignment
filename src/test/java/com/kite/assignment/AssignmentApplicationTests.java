package com.kite.assignment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.kite.assignment.controllers.AssignmentController;
import com.kite.assignment.model.Assignment;
import com.kite.assignment.model.AssignmentResponse;
import com.kite.assignment.model.ObjectiveQuestion;
import com.kite.assignment.model.Question;
import com.kite.assignment.model.STATUS;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AssignmentApplicationTests {
	
	private String assignmentId = "testAssignmentId";
	
	private String questionId = "testQuestion";
	
	private String objectiveQuestionId = "testObjectiveQuestion";
	
	@Autowired
	private AssignmentController assignmentController;
	
	@Autowired
	private Bootstrap bootstrap;
	
	@Test
	public void contextLoads() {
		
	}
	
	@Test
	public void testCreateAssignment() {
		ResponseEntity<AssignmentResponse> response = assignmentController.createAssignment();
		assertEquals(response.getStatusCode(), HttpStatus.CREATED);
		assertFalse(!response.getBody().isStatus());
	}
	
	@Before
	public void before() {
		if(!AssignmentApplication.init) {
			Assignment assignment = new Assignment(this.assignmentId);
			assignment.setStatus(STATUS.ACTIVE.getStatus());
			Question q = new Question("Write anything.");
			q.setQuestionId(this.questionId);
			assignment.addQuestions(q);
			ObjectiveQuestion ques = new ObjectiveQuestion();
			ques.setQuestionId(this.objectiveQuestionId);
			ques.setQuestion("Sun Rises from ?");
			ques.addOption("East");
			ques.addOption("West");
			ques.addOption("North");
			ques.addOption("South");
			assignment.addQuestions(ques);
			bootstrap.add(assignment);
			AssignmentApplication.init = true;
		}
	}
	
	@Test
	public void testDeleteAssignment() {
		assertEquals(assignmentController.deleteAssignment(assignmentController.createAssignment().getBody().getAssignment().getAssignmentId()).getStatusCode(), HttpStatus.BAD_REQUEST);
	}
	
	@Test
	public void testGetAssignment() {
		assertEquals(assignmentController.getAssignment(assignmentId).getStatusCode(), HttpStatus.OK);
	}
	
	@Test
	public void testAddQuestion() {
		assertEquals(assignmentController.addQuestion(assignmentId, "Hello Write Anything...").getStatusCode(), HttpStatus.OK);
	}
	
	@Test
	public void testAddObjectiveQuestion() {
		Set<String> options = new HashSet<>();
		options.add("option a");
		options.add("option b");
		options.add("option c");
		options.add("option d");
		ObjectiveQuestion q = new ObjectiveQuestion("Test Ques", options);
		assertEquals(assignmentController.addObjectiveQuestion(this.assignmentId, q).getStatusCode(), HttpStatus.OK);
	}
	
	@Test
	public void testRemoveQuestion() {
		assertEquals(assignmentController.removeQuestion(this.assignmentId, this.objectiveQuestionId).getStatusCode(), HttpStatus.OK);
		assertEquals(assignmentController.removeQuestion(this.assignmentId, this.questionId).getStatusCode(), HttpStatus.OK);
	}
	
	@Test
	public void testDisableAssignment() {
		assertEquals(assignmentController.disableAssignment(assignmentController.createAssignment().getBody().getAssignment().getAssignmentId()).getStatusCode(), HttpStatus.OK);
	}

}
