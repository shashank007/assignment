package com.kite.assignment.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.Date;

import com.kite.assignment.exception.ResponseException;
import com.kite.assignment.model.Assignment;
import com.kite.assignment.model.AssignmentResponse;

public class Utils {
	
	public static boolean isNull(Object obj) {
		return obj == null;
	}
	
	public static boolean isNull(String obj) {
		return obj == null || obj.trim().isEmpty() || obj.trim().equals("");
	}
	
	public static String getStackTrace(Exception e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		String exceptionMessage = sw.toString();
		pw.close();
		return exceptionMessage;
	}
	
	public static AssignmentResponse successResponse(String message) {
		AssignmentResponse assignmentResponse = new AssignmentResponse(message);
		assignmentResponse.setStatus(true);
		return assignmentResponse;
	}
	
	public static AssignmentResponse successResponse(Assignment assignment) {
		return new AssignmentResponse(assignment);
	}
	
	public static AssignmentResponse failureResponse(String message) {
		return new AssignmentResponse(message);
	}

	public static void validateDate(Date createdAt) throws ResponseException {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(createdAt);
		calendar.add(Calendar.MONTH, 1);
		if(calendar.getTime().after(new Date())) {
			throw new ResponseException("Assignment is not Older than 1 Month");
		}
	}

}
