package com.kite.assignment.model;

import java.util.HashSet;
import java.util.Set;

public class ObjectiveQuestion extends Question {

	private Set<String> options;
	
	public ObjectiveQuestion() {
		super();
		this.isObjectiveType = true;
	}
	
	public ObjectiveQuestion(String questionId) {
		super(questionId, true);
	}
	
	public ObjectiveQuestion(String question,Set<String> options) {
		super(question);
		this.isObjectiveType = true;
		this.options = options;
	}

	public Set<String> getOptions() {
		return options;
	}

	public void setOptions(Set<String> options) {
		this.options = options;
	}
	
	public boolean addOption(String option) {
		if(this.options == null)
			this.options = new HashSet<>();
		return this.options.add(option);
	}

	@Override
	public String toString() {
		return "ObjectiveQuestion [options=" + options + ", questionId=" + questionId + ", question=" + question
				+ ", isObjectiveType=" + isObjectiveType + "]";
	}
}

