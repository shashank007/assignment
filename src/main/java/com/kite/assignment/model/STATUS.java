package com.kite.assignment.model;

public enum STATUS {
	
	ACTIVE("active"),
	
	INACTIVE("inActive");
	
	private String status;

	private STATUS(String status) {
		this.status = status;
	}
	
	public String getStatus() {
		return this.status;
	}

}
