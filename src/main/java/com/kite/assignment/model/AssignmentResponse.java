package com.kite.assignment.model;

public class AssignmentResponse {
	
	private boolean status = false;
	
	private String message;
	
	private Assignment assignment;

	public AssignmentResponse(Assignment assignment) {
		super();
		this.status = true;
		this.assignment = assignment;
	}

	public AssignmentResponse(String message) {
		super();
		this.message = message;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Assignment getAssignment() {
		return assignment;
	}

	public void setAssignment(Assignment assignment) {
		this.assignment = assignment;
	}
}
