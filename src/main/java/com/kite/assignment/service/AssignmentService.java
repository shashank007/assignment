package com.kite.assignment.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kite.assignment.Bootstrap;
import com.kite.assignment.exception.ResponseException;
import com.kite.assignment.model.Assignment;
import com.kite.assignment.model.ObjectiveQuestion;
import com.kite.assignment.model.Question;
import com.kite.assignment.model.STATUS;
import com.kite.assignment.util.Utils;

@Service
public class AssignmentService implements IAssignmentService{
	
	@Autowired
	private Bootstrap bootstrap;

	@Override
	public Assignment createAssignment() throws ResponseException {
		try {
			Assignment assignment = new Assignment();
			assignment.setStatus(STATUS.ACTIVE.getStatus());
			bootstrap.add(assignment);
			return assignment;
		}catch (Exception e) {
			throw new ResponseException(e);
		}
	}

	@Override
	public Assignment getAssignment(String assignmentId) throws ResponseException {
		try {
			if(Utils.isNull(assignmentId)) {
				throw new ResponseException("Assignment Id is Null.");
			}
			Assignment assignment = bootstrap.get(new Assignment(assignmentId));
			if(Utils.isNull(assignment)) {
				throw new ResponseException("No Assignment found by id "+assignmentId);
			}
			if(assignment.getStatus().equals(STATUS.INACTIVE.getStatus())) {
				throw new ResponseException("Assignment Found by id "+assignmentId+" is Disabled.");
			}
			return assignment;
		}catch (Exception e) {
			throw new ResponseException(e);
		}
	}

	@Override
	public Assignment disableAssignment(String assignmentId) throws ResponseException {
		try {
			if(Utils.isNull(assignmentId)) {
				throw new ResponseException("Assignment Id is Null.");
			}
			Assignment assignment = bootstrap.get(new Assignment(assignmentId));
			if(Utils.isNull(assignment)) {
				throw new ResponseException("No Assignment found by id "+assignmentId);
			}
			if(assignment.getStatus().equals(STATUS.INACTIVE.getStatus())) {
				throw new ResponseException("Assignment Found by id "+assignmentId+" is Already Disabled.");
			}
			assignment.setStatus(STATUS.INACTIVE.getStatus());
			assignment.setUpdatedAt(new Date());
			return assignment;
		}catch (Exception e) {
			throw new ResponseException(e);
		}
	}

	@Override
	public Assignment addQuestion(String assignmentId, String question) throws ResponseException {
		try {
			if(Utils.isNull(assignmentId)) {
				throw new ResponseException("Assignment Id is Null.");
			}
			if(Utils.isNull(question)) {
				throw new ResponseException("question is Null.");
			}
			Assignment assignment = bootstrap.get(new Assignment(assignmentId));
			if(Utils.isNull(assignment)) {
				throw new ResponseException("No Assignment found by id "+assignmentId);
			}
			if(assignment.getStatus().equals(STATUS.INACTIVE.getStatus())) {
				throw new ResponseException("Assignment Found by id "+assignmentId+" is Disabled.");
			}
			assignment.addQuestions(new Question(question));
			assignment.setUpdatedAt(new Date());
			return assignment;
		}catch (Exception e) {
			throw new ResponseException(e);
		}
	}

	@Override
	public Assignment removeQuestion(String assignmentId, String questionId) throws ResponseException {
		try {
			if(Utils.isNull(assignmentId)) {
				throw new ResponseException("Assignment Id is Null.");
			}
			if(Utils.isNull(questionId)) {
				throw new ResponseException("questionId is Null.");
			}
			Assignment assignment = bootstrap.get(new Assignment(assignmentId));
			if(Utils.isNull(assignment)) {
				throw new ResponseException("No Assignment found by id "+assignmentId);
			}
			if(assignment.getStatus().equals(STATUS.INACTIVE.getStatus())) {
				throw new ResponseException("Assignment Found by id "+assignmentId+" is Disabled.");
			}
			List<Question> questions = assignment.getQuestions();
			if(Utils.isNull(questions) || questions.isEmpty()) {
				throw new ResponseException("No Questions Found in Assignment with id "+assignmentId);
			}
			int index = questions.indexOf(new Question(questionId,true));
			if(index == -1) {
				index = questions.indexOf(new ObjectiveQuestion(questionId));
				if(index == -1) {
					throw new ResponseException("Question with id "+questionId+" Not Found in Assignment with id "+assignmentId);
				}
			}
			questions.remove(index);
			assignment.setUpdatedAt(new Date());
			return assignment;
		}catch (Exception e) {
			throw new ResponseException(e);
		}
	}

	@Override
	public Assignment addQuestion(String assignmentId, ObjectiveQuestion question) throws ResponseException {
		try {
			if(Utils.isNull(assignmentId)) {
				throw new ResponseException("Assignment Id is Null.");
			}
			if(Utils.isNull(question)) {
				throw new ResponseException("question is Null.");
			}
			Assignment assignment = bootstrap.get(new Assignment(assignmentId));
			if(Utils.isNull(assignment)) {
				throw new ResponseException("No Assignment found by id "+assignmentId);
			}
			if(assignment.getStatus().equals(STATUS.INACTIVE.getStatus())) {
				throw new ResponseException("Assignment Found by id "+assignmentId+" is Disabled.");
			}
			assignment.addQuestions(question);
			assignment.setUpdatedAt(new Date());
			return assignment;
		}catch (Exception e) {
			throw new ResponseException(e);
		}
	}

	@Override
	public void delete(String assignmentId) throws ResponseException {
		try {
			if(Utils.isNull(assignmentId)) {
				throw new ResponseException("Assignment Id is Null.");
			}
			Assignment assignment = bootstrap.get(new Assignment(assignmentId));
			if(Utils.isNull(assignment)) {
				throw new ResponseException("No Assignment found by id "+assignmentId);
			}
			Utils.validateDate(assignment.getCreatedAt());
			bootstrap.remove(assignment);
		}catch (Exception e) {
			throw new ResponseException(e);
		}
	}
}
