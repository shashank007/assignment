package com.kite.assignment.service;

import com.kite.assignment.exception.ResponseException;
import com.kite.assignment.model.Assignment;
import com.kite.assignment.model.ObjectiveQuestion;

public interface IAssignmentService {

	public Assignment createAssignment() throws ResponseException;

	public Assignment getAssignment(String assignmentId) throws ResponseException;

	public Assignment disableAssignment(String assignmentId) throws ResponseException;

	public Assignment addQuestion(String assignmentId, String question) throws ResponseException;

	public Assignment removeQuestion(String assignmentId, String questionId) throws ResponseException;

	public Assignment addQuestion(String assignmentId, ObjectiveQuestion question) throws ResponseException;

	public void delete(String assignmentId) throws ResponseException;
	
}
