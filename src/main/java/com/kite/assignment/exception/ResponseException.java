package com.kite.assignment.exception;

public class ResponseException extends Exception {

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	public ResponseException() {
		super();
	}

	public ResponseException(String message) {
		super(message);
	}

	public ResponseException(Throwable cause) {
		super(cause);
	}

	public ResponseException(String message, Throwable cause) {
		super(message, cause);
	}

}
