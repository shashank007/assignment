package com.kite.assignment;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.kite.assignment.model.Assignment;

@Component
public class Bootstrap {
	
	private List<Assignment> assignments;
	
	@PostConstruct
	public void init() {
		assignments = new ArrayList<>();
	}
	
	public List<Assignment> getAssignments(){
		return assignments;
	}
	
	public void add(Assignment assignment) {
		int index = assignments.indexOf(assignment);
		if(index != -1) {
			assignments.remove(index);
		}
		assignments.add(assignment);
	}
	
	public Assignment get(Assignment assignment) {
		int index = assignments.indexOf(assignment);
		if(index == -1) {
			return null;
		}
		return assignments.get(index);
	}
	
	public Assignment remove(Assignment assignment) {
		int index = assignments.indexOf(assignment);
		if(index == -1) {
			return null;
		}
		return assignments.remove(index);
	}
}
