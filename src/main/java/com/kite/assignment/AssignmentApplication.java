package com.kite.assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssignmentApplication {
	
	public static boolean init = false;

	public static void main(String[] args) {
		SpringApplication.run(AssignmentApplication.class, args);
	}
}
