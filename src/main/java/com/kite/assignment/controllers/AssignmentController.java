package com.kite.assignment.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kite.assignment.exception.ResponseException;
import com.kite.assignment.model.AssignmentResponse;
import com.kite.assignment.model.ObjectiveQuestion;
import com.kite.assignment.service.IAssignmentService;
import com.kite.assignment.util.Utils;

@RestController
@RequestMapping("/assignment")
public class AssignmentController {
	
	@Autowired
	private IAssignmentService iAssignmentService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AssignmentController.class);
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<AssignmentResponse> createAssignment() {
		LOGGER.debug("In AssignmentController createAssignment");
		try {
			return new ResponseEntity<>(Utils.successResponse(iAssignmentService.createAssignment()),HttpStatus.CREATED);
		}catch (ResponseException e) {
			LOGGER.error("ResponseException occured during Assignment Creation "+e.getMessage());
			LOGGER.error("ResponseException Stack Trace "+Utils.getStackTrace(e));
			return new ResponseEntity<>(Utils.failureResponse(e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}catch (Exception e) {
			LOGGER.error("Exception occured during Assignment Creation "+e.getMessage());
			LOGGER.error("Exception Stack Trace "+Utils.getStackTrace(e));
			return new ResponseEntity<>(Utils.failureResponse(e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<AssignmentResponse> getAssignment(@PathVariable("id") String assignmentId) {
		LOGGER.debug("In AssignmentController getAssignment");
		LOGGER.debug("assignmentId = "+assignmentId);
		try {
			return new ResponseEntity<>(Utils.successResponse(iAssignmentService.getAssignment(assignmentId)),HttpStatus.OK);
		}catch (ResponseException e) {
			LOGGER.error("ResponseException occured during Assignment Creation "+e.getMessage());
			LOGGER.error("ResponseException Stack Trace "+Utils.getStackTrace(e));
			return new ResponseEntity<>(Utils.failureResponse(e.getMessage()),HttpStatus.BAD_REQUEST);
		}catch (Exception e) {
			LOGGER.error("Exception occured during Assignment Creation "+e.getMessage());
			LOGGER.error("Exception Stack Trace "+Utils.getStackTrace(e));
			return new ResponseEntity<>(Utils.failureResponse(e.getMessage()),HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value = "/{id}/disable", method = RequestMethod.GET)
	public ResponseEntity<AssignmentResponse> disableAssignment(@PathVariable("id") String assignmentId) {
		LOGGER.debug("In AssignmentController disableAssignment");
		LOGGER.debug("assignmentId = "+assignmentId);
		try {
			return new ResponseEntity<>(Utils.successResponse(iAssignmentService.disableAssignment(assignmentId)),HttpStatus.OK);
		}catch (ResponseException e) {
			LOGGER.error("ResponseException occured during Assignment Creation "+e.getMessage());
			LOGGER.error("ResponseException Stack Trace "+Utils.getStackTrace(e));
			return new ResponseEntity<>(Utils.failureResponse(e.getMessage()),HttpStatus.BAD_REQUEST);
		}catch (Exception e) {
			LOGGER.error("Exception occured during Assignment Creation "+e.getMessage());
			LOGGER.error("Exception Stack Trace "+Utils.getStackTrace(e));
			return new ResponseEntity<>(Utils.failureResponse(e.getMessage()),HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value = "/{id}/question/add", method = RequestMethod.POST)
	public ResponseEntity<AssignmentResponse> addQuestion(@PathVariable("id") String assignmentId,@RequestParam("question") String question) {
		LOGGER.debug("In AssignmentController addQuestion");
		LOGGER.debug("assignmentId = "+assignmentId);
		LOGGER.debug("assignmentId = "+question);
		try {
			return new ResponseEntity<>(Utils.successResponse(iAssignmentService.addQuestion(assignmentId,question)),HttpStatus.OK);
		}catch (ResponseException e) {
			LOGGER.error("ResponseException occured during Assignment Creation "+e.getMessage());
			LOGGER.error("ResponseException Stack Trace "+Utils.getStackTrace(e));
			return new ResponseEntity<>(Utils.failureResponse(e.getMessage()),HttpStatus.BAD_REQUEST);
		}catch (Exception e) {
			LOGGER.error("Exception occured during Assignment Creation "+e.getMessage());
			LOGGER.error("Exception Stack Trace "+Utils.getStackTrace(e));
			return new ResponseEntity<>(Utils.failureResponse(e.getMessage()),HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value = "/{id}/question/remove/{questionId}", method = RequestMethod.DELETE)
	public ResponseEntity<AssignmentResponse> removeQuestion(@PathVariable("id") String assignmentId,@PathVariable("questionId") String questionId) {
		LOGGER.debug("In AssignmentController removeQuestion");
		LOGGER.debug("assignmentId = "+assignmentId);
		LOGGER.debug("assignmentId = "+questionId);
		try {
			return new ResponseEntity<>(Utils.successResponse(iAssignmentService.removeQuestion(assignmentId,questionId)),HttpStatus.OK);
		}catch (ResponseException e) {
			LOGGER.error("ResponseException occured during Assignment Creation "+e.getMessage());
			LOGGER.error("ResponseException Stack Trace "+Utils.getStackTrace(e));
			return new ResponseEntity<>(Utils.failureResponse(e.getMessage()),HttpStatus.BAD_REQUEST);
		}catch (Exception e) {
			LOGGER.error("Exception occured during Assignment Creation "+e.getMessage());
			LOGGER.error("Exception Stack Trace "+Utils.getStackTrace(e));
			return new ResponseEntity<>(Utils.failureResponse(e.getMessage()),HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value = "/{id}/question/objective/add", method = RequestMethod.POST)
	public ResponseEntity<AssignmentResponse> addObjectiveQuestion(@PathVariable("id") String assignmentId,@RequestBody ObjectiveQuestion question) {
		LOGGER.debug("In AssignmentController addObjectiveQuestion");
		LOGGER.debug("assignmentId = "+assignmentId);
		LOGGER.debug("assignmentId = "+question);
		try {
			return new ResponseEntity<>(Utils.successResponse(iAssignmentService.addQuestion(assignmentId,question)),HttpStatus.OK);
		}catch (ResponseException e) {
			LOGGER.error("ResponseException occured during Assignment Creation "+e.getMessage());
			LOGGER.error("ResponseException Stack Trace "+Utils.getStackTrace(e));
			return new ResponseEntity<>(Utils.failureResponse(e.getMessage()),HttpStatus.BAD_REQUEST);
		}catch (Exception e) {
			LOGGER.error("Exception occured during Assignment Creation "+e.getMessage());
			LOGGER.error("Exception Stack Trace "+Utils.getStackTrace(e));
			return new ResponseEntity<>(Utils.failureResponse(e.getMessage()),HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<AssignmentResponse> deleteAssignment(@PathVariable("id") String assignmentId) {
		LOGGER.debug("In AssignmentController deleteAssignment");
		LOGGER.debug("assignmentId = "+assignmentId);
		try {
			iAssignmentService.delete(assignmentId);
			return new ResponseEntity<>(Utils.successResponse("Successfully Deleted Assignment."),HttpStatus.OK);
		}catch (ResponseException e) {
			LOGGER.error("ResponseException occured during Assignment Creation "+e.getMessage());
			LOGGER.error("ResponseException Stack Trace "+Utils.getStackTrace(e));
			return new ResponseEntity<>(Utils.failureResponse(e.getMessage()),HttpStatus.BAD_REQUEST);
		}catch (Exception e) {
			LOGGER.error("Exception occured during Assignment Creation "+e.getMessage());
			LOGGER.error("Exception Stack Trace "+Utils.getStackTrace(e));
			return new ResponseEntity<>(Utils.failureResponse(e.getMessage()),HttpStatus.BAD_REQUEST);
		}
	}

}
